import matplotlib.colors
import networkx


def rotr(x: int, n: int, size: int) -> int:
    """circular shift of `x` right by `n`. 001 -> 100"""
    return int(
        ((x & (2 ** size - 1)) >> n % size)
        | (x << (size - (n % size)) & (2 ** size - 1))
    )


def rotl(x: int, n: int, size: int) -> int:
    """circular shift of `x` left by `n`. 100 -> 001"""
    return int(
        (x << n % size) & (2 ** size - 1)
        | ((x & (2 ** size - 1)) >> (size - (n % size)))
    )


def colorize(
    graph: networkx.Graph,
    period: int,
    attr: str = "color",
) -> None:
    colors = list(matplotlib.colors.CSS4_COLORS)
    for n in graph.nodes:
        graph.nodes[n][attr] = colors[n // period % len(colors)]
