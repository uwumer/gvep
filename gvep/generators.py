import itertools

import networkx
import networkx.generators.classic
from networkx.algorithms.operators.all import (
    disjoint_union_all,
)

from gvep.utils import (
    rotr,
)

MeshSize = tuple[int, int]

complete_graph = networkx.complete_graph
circulant_graph = networkx.circulant_graph
grid_2d_graph = networkx.grid_2d_graph


def shifted_route_graph(groups: int, gsize: int) -> networkx.DiGraph:
    bsize = int(gsize).bit_length()
    graph = networkx.DiGraph()
    for (g1, g2) in itertools.combinations(range(groups), 2):
        d = g2 - g1
        n1 = g1 * gsize + g2 % gsize
        n2 = g2 * gsize + (rotr(n1 % gsize + 1, d, bsize) - 1) % gsize
        graph.add_edge(g1, g2, node=n1)
        graph.add_edge(g2, g1, node=n2)
    return graph


def flat_route_graph(route_graph: networkx.DiGraph) -> networkx.Graph:
    graph = networkx.Graph()
    for g1, g2, n1 in route_graph.edges(data="node"):
        n2 = route_graph.get_edge_data(g2, g1)["node"]
        graph.add_edge(n1, n2)

    return graph


def chip_level(groups: int, base: networkx.Graph) -> networkx.Graph:
    route_graph = shifted_route_graph(groups, len(base))
    graph: networkx.Graph = networkx.compose(
        disjoint_union_all(base.copy() for _ in range(groups)),
        flat_route_graph(route_graph),
    )
    return graph
