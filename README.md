# Graph Viewer Extreme 2d Pro

## Documentation

https://uwumer.gitlab.io/gvep/

## Contributing

Create a virtual environment with python~=3.10.

```shell
$ python --version
Python 3.10.0
$ python -m venv .venv
$ source .venv/bin/activate
```

Clone the repo, install dev tools for git hooks, code formatting, and linting.

```shell
$ git clone git@gitlab.com:uwumer/gvep.git
$ cd gvep
$ python -m pip install -r requirements-dev.txt
$ pre-commit install
```

Run `black` and `isort` to autoformat code. `pre-commit` should also take care of that.

```shell
$ black gvep test
$ isort gvep test
```

Ensure `mypy` and `flake8` return no errors before committing.

```shell
$ mypy gvep test --install-types --non-interactive
$ flake8 gvep test
```

Use `feature/`, `fix/`, or similar prefixes for branch names.
