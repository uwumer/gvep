import pytest

from gvep import (
    utils,
)


@pytest.mark.parametrize(
    "x,y,n",
    [
        ("110", "011", 1),
        ("110", "101", 2),
    ],
)
def test_rotr(x: str, y: str, n: int) -> None:
    assert int(y, 2) == utils.rotr(int(x, 2), n, len(x))


@pytest.mark.parametrize(
    "x,y,n",
    [
        ("110", "101", 1),
        ("110", "011", 2),
    ],
)
def test_rotl(x: str, y: str, n: int) -> None:
    assert int(y, 2) == utils.rotl(int(x, 2), n, len(x))
