import networkx
import pytest

from gvep import (
    generators,
)


@pytest.mark.parametrize("groups", range(1, 5))
@pytest.mark.parametrize("gsize", range(1, 7))
def test_flat_route_graph(groups: int, gsize: int) -> None:
    route_graph = generators.shifted_route_graph(groups, gsize)
    flat_graph = generators.flat_route_graph(route_graph)
    inv_flat_graph = networkx.DiGraph()
    for n1, n2 in flat_graph.edges:
        inv_flat_graph.add_edge(n1 // gsize, n2 // gsize, node=n1)
        inv_flat_graph.add_edge(n2 // gsize, n1 // gsize, node=n2)
    assert networkx.is_isomorphic(inv_flat_graph, route_graph)


@pytest.mark.parametrize("groups", range(1, 5))
def test_chip_level(groups: int) -> None:
    base = generators.complete_graph(4)
    graph = generators.chip_level(groups, base)
    assert networkx.is_connected(graph)
